<?php
namespace App\Model;

use App\Config\Config;

class TaxReport implements \JsonSerializable
{
    /** @var string */
	public $method;

    /** @var int */
	public $year;

    /** @var int */
	public $percent;

    /** @var int */
	private $sold;

    /** @var int */
	private $cost;

    /** @var int */
	private $proceeds;

    /** @var int */
	private $gain;

    /** @var int */
	private $tax;

	public function __construct(string $method, int $percent, int $year)
	{
    	$this->method = $method;
    	$this->percent = $percent;
    	$this->year = $year;
    	$this->sold = 0;
    	$this->cost = 0;
    	$this->proceeds = 0;
    	$this->gain = 0;
    	$this->tax = 0;
	}

    /**
     * Needed mainly for testing.
     */
    public function getTax() : int
    {
        return $this->tax;
    }

    /**
     * Calculates taxes based on provided parameters and all known transactions.
     */
	public function calculate(array $transactions) : void
	{
        $buyPools = [];
    	$sellPools = [];
    	foreach ($transactions as $transaction) {
        	if ($transaction->type != 'Trade') {
            	continue;
        	}
        	$year = (int) date('Y', $transaction->date / Config::$MILLIS);
        	if ($transaction->buy_currency == 'BTC') {
            	$buyPools[$year][] = $transaction;
            	continue;
        	}
        	if ($transaction->sell_currency == 'BTC') {
            	$sellPools[$year][] = $transaction;
        	}
    	}

        foreach ($sellPools as &$sellPool) {
        	\usort($sellPool, function (Transaction $a, Transaction $b) {
            	return $a->date <=> $b->date;
        	});
        }

    	foreach ($buyPools as &$buyPool) {
        	if ($this->method == Config::$FIFO_METHOD) {
            	\usort($buyPool, function (Transaction $a, Transaction $b) {
                	return $a->date <=> $b->date;
            	});
        	} else {
            	\usort($buyPool, function (Transaction $a, Transaction $b) {
                	return $b->date <=> $a->date;
            	});
        	}
    	}

    	foreach ($sellPools[$this->year] ?? [] as $transaction) {
        	$this->sold += $transaction->sell;
        	$this->proceeds += $transaction->buy;
    	}

    	$sold = $this->sold;
    	foreach ($buyPools[$this->year] ?? [] as $transaction) {
        	$sold -= $transaction->buy;
        	if ($sold > 0) {
            	$this->cost += $transaction->sell;
        	} else if ($sold == 0) {
            	$this->cost += $transaction->sell;
            	break;
        	} else {
            	$this->cost += $transaction->sell *
            	    ($transaction->buy + $sold) / $transaction->buy;
            	break;
        	}
    	}

    	$this->gain = $this->proceeds - $this->cost;
    	$this->tax = (int) ($this->gain * ($this->percent / 100));
	}

	public function jsonSerialize() : array
	{
    	return [
        	'method' => $this->method,
        	'year' => $this->year,
        	'percent' => $this->percent,
        	'sold' => $this->sold,
        	'cost' => $this->cost,
        	'proceeds' => $this->proceeds,
        	'gain' => $this->gain,
        	'tax' => $this->tax,
    	];
	}
}
