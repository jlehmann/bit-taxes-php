<?php
namespace App\Model;

use Ramsey\Uuid\Uuid;
use App\Config\Config;
use App\Validator\Csv as Validator;

class Transaction
{
    /** @var string */
    public $id;

    /** @var string */
    public $type;

    /** @var int */
    public $buy;

    /** @var string */
    public $buy_currency;

    /** @var int */
    public $sell;

    /** @var string */
    public $sell_currency;

    /** @var int */
    public $fee;

    /** @var string */
    public $fee_currency;

    /** @var string */
    public $exchange;

    /** @var string */
    public $group;

    /** @var string */
    public $comment;

    /** @var int */
    public $date;

    /**
     * Creates Transaction from provided CSV row.
     * Multiplies prices by constant to store prices in integers.
     */
    public static function fromCsvRow(array $csvRow) : self
    {
        if (!Validator::isValidCsvRow($csvRow)) {
            throw new \Exception('invalid CSV row');
        }

        $transaction = new Transaction();

        $transaction->id = (string) Uuid::uuid4();
        $transaction->type = $csvRow[0];
        $buy = empty($csvRow[1]) ? 0 : $csvRow[1];
        $transaction->buy = $buy * Config::$PRECISION;
        $transaction->buy_currency = $csvRow[2];
        $sell = empty($csvRow[3]) ? 0 : $csvRow[3];
        $transaction->sell = $sell * Config::$PRECISION;
        $transaction->sell_currency = $csvRow[4];
        $fee = empty($csvRow[5]) ? 0 : $csvRow[5];
        $transaction->fee = $fee * Config::$PRECISION;
        $transaction->fee_currency = $csvRow[6];
        $transaction->exchange = $csvRow[7];
        $transaction->group = $csvRow[8];
        $transaction->comment = $csvRow[9];
        // Convert timestamp to millis
        $date = strtotime($csvRow[10]) * Config::$MILLIS;
        $transaction->date = $date;

        return $transaction;
    }

    /**
     * Creates Transaction object from database row.
     */
    public static function fromResultRow(array $resultRow) : self
    {
        $transaction = new Transaction();

        $transaction->id = $resultRow['id'];
        $transaction->type = $resultRow['type'];
        $transaction->buy = (int) $resultRow['buy'];
        $transaction->buy_currency = $resultRow['buy_currency'];
        $transaction->sell = (int) $resultRow['sell'];
        $transaction->sell_currency = $resultRow['sell_currency'];
        $transaction->fee = (int) $resultRow['fee'];
        $transaction->fee_currency = $resultRow['fee_currency'];
        $transaction->exchange = $resultRow['exchange'];
        $transaction->group = $resultRow['group_'];
        $transaction->comment = $resultRow['comment'];
        $transaction->date = (int) $resultRow['date'];

        return $transaction;
    }

    /**
     * Returns transaction object as array.
     */
    public function toArray() : array
    {
        return \get_object_vars($this);
    }
}
