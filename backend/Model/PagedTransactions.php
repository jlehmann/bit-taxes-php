<?php
namespace App\Model;

class PagedTransactions
{
    /** @var \App\Model\Transaction[] */
    public $transactions;

    /** @var int */
    public $pages;

    /** @var int */
    public $current_page;

    /**
     * Creates PagedTransactions object from provided parameters.
     */
    public static function fromTransactions(
        array $transactions,
        int $pages,
        int $currentPage
    ) : self {
        $pagedTransactions = new PagedTransactions();

        $pagedTransactions->transactions = $transactions;
        $pagedTransactions->pages = $pages;
        $pagedTransactions->current_page = $currentPage;

        return $pagedTransactions;
    }
}
