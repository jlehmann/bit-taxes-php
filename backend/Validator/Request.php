<?php
namespace App\Validator;

use App\Config\Config;

class Request
{
    /**
     * Validates received method name.
     */
    public static function isValidMethod(string $method) : bool
    {
        return in_array($method, [Config::$FIFO_METHOD, Config::$LIFO_METHOD]);
    }
}
