<?php
namespace App\Validator;

use App\Config\Config;

class Csv
{
    /**
     * Validates if CSV row is in valid and accepted format.
     */
    public static function isValidCsvRow(array $csvRow) : bool
    {
        return !empty($csvRow[0]) && count($csvRow) == Config::$CSV_COLUMNS;
    }
}
