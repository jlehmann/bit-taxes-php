<?php
namespace App\Config;

class Config
{
    public static $DB_FILE = './../database/storage.sqlite';
    public static $PAGE_SIZE = 10;
    public static $PRECISION = 100000000;
    public static $CSV_COLUMNS = 11;
    public static $MILLIS = 1000;
    public static $FIFO_METHOD = 'FIFO';
    public static $LIFO_METHOD = 'LIFO';
}
