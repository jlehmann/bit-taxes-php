<?php
namespace App\Controller;

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Psr7\Request;
use App\Model\TaxReport;
use App\Validator\Request as RequestValidator;

class TaxReportController
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;

        if (!$this->container->has('db')) {
            throw new \Exception('no db initialized inside container');
        }
    }

    /**
     * Handles tax report response based on provided query params.
     */
    public function __invoke(Request $request, Response $response, array $args) : Response
    {
        $queryParams = $request->getQueryParams();

        $method = $queryParams['method'] ?? '';
        $percent = (int) ($queryParams['percent'] ?? 0);
        $year = (int) ($queryParams['year'] ?? 0);

        if (!RequestValidator::isValidMethod($method) || $percent <= 0 || $year <= 0) {
            return $response->withStatus(400);
        }

        $db = $this->container->get('db');
        $transactions = $db->getTransactions();

        $taxReport = new TaxReport($method, $percent, $year);
        $taxReport->calculate($transactions);

        $response->getBody()->write(\json_encode($taxReport));

        return $response->withHeader('Content-Type', 'application/json');
    }
}
