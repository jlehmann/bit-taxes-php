<?php
namespace App\Controller;

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\RequestInterface as Request;
use App\Model\Transaction;

class CsvController
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;

        if (!$this->container->has('db')) {
            throw new \Exception('no db initialized inside container');
        }
    }

    /**
     * Handles importing of transactions from CSV file.
     */
    public function __invoke(Request $request, Response $response, array $args) : Response
    {
        $contentType = $request->getHeader("Content-Type");
        if (empty($contentType[0]) || $contentType[0] != "text/csv") {
            return $response->withStatus(400);
        }

        $csvRows = explode("\n", $request->getBody()->getContents());
        if (empty($csvRows)) {
            return $response->withStatus(400);
        }

        // skip header line
        $csvRows = array_slice($csvRows, 1);
        $csvRows = array_map('str_getcsv', $csvRows);

        $transactions = [];
        foreach ($csvRows as $csvRow) {
            try {
                $transactions[] = Transaction::fromCsvRow($csvRow);
            } catch (\Exception $e) {
                error_log($e->getMessage());
            }
        }

        if (empty($transactions)) {
            return $response->withStatus(400);
        }

        $db = $this->container->get('db');
        $db->saveTransactions($transactions);

        return $response->withStatus(200);
    }
}
