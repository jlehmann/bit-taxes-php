<?php
namespace App\Controller;

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Psr7\Request;

class TransactionsController
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Handles returning of paginated transactions.
     */
    public function __invoke(Request $request, Response $response, array $args) : Response
    {
        if (!$this->container->has('db')) {
            throw new \Exception('no db initialized inside container');
        }

        $page = $request->getQueryParams()['page'] ?? 1;
        if ($page < 1) {
            $page = 1;
        }

        $db = $this->container->get('db');
        $pagedTransactions = $db->getPagedTransactions($page);

        $response->getBody()->write(\json_encode($pagedTransactions));

        return $response->withHeader('Content-Type', 'application/json');
    }
}
