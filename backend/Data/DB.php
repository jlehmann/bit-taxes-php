<?php
namespace App\Data;

use App\Config\Config;
use App\Model\Transaction;
use App\Model\PagedTransactions;

class DB
{
    private $connection;

    public function __construct(string $dbFile)
    {
        if (empty($dbFile)) {
            throw new \Exception("cannot open empty file name");
        }
        $this->connection = new \PDO('sqlite:'.$dbFile);
        $this->connection->setAttribute(
            \PDO::ATTR_ERRMODE,
            \PDO::ERRMODE_EXCEPTION
        );
        if (empty($this->connection)) {
            throw new \Exception("cannot open database connection");
        }
    }

    /**
     * Returns number of transaction pages available in database.
     */
    private function getPages() : int
    {
        $countQuery = "SELECT COUNT(*) FROM transactions ORDER BY date DESC;";
        $countStmt = $this->connection->prepare($countQuery);
        $countStmt->execute();
        $count = (int) $countStmt->fetch()[0] ?? 0;

        $pages = ceil($count / Config::$PAGE_SIZE);

        return $pages;
    }

    /**
     * Returns requested page of transactions.
     */
    public function getPagedTransactions(int $page) : PagedTransactions
    {
        $offsetParam = $page - 1;
        if ($offsetParam < 0) {
            $offsetParam = 0;
        }
        $query = "SELECT * FROM transactions ORDER BY date DESC LIMIT ? OFFSET ?;";
        $stmt = $this->connection->prepare($query);
        $stmt->execute([Config::$PAGE_SIZE, $offsetParam * Config::$PAGE_SIZE]);
        $result = $stmt->fetchAll();

        $transactions = [];
        foreach ($result as $resultRow) {
            $transactions[] = Transaction::fromResultRow($resultRow);
        }

        $pages = $this->getPages();

        return PagedTransactions::fromTransactions($transactions, $pages, $page);
    }

    /**
     * Returns all transactions available in database.
     */
    public function getTransactions() : array
    {
        $query = "SELECT * FROM transactions;";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        $result = $stmt->fetchAll();

        $transactions = [];
        foreach ($result as $resultRow) {
            $transactions[] = Transaction::fromResultRow($resultRow);
        }

        return $transactions;
    }

    /**
     * Saves provided transactions in database.
     * It deletes the old ones.
     */
    public function saveTransactions(array $transactions) : void
    {
        $this->connection->beginTransaction();

        try {
            $this->deleteTransactions();
        } catch (\PDOException $e) {
            $this->connection->rollBack();
            throw $e;
        }

        $query = "INSERT INTO transactions VALUES ";

        $transactionChunks = \array_chunk($transactions, 50);
        foreach ($transactionChunks as $transactionChunk) {
            $placeHolers = [];
            $values = [];
            foreach ($transactionChunk as $transaction) {
                $placeHolers[] = "(?,?,?,?,?,?,?,?,?,?,?,?)";
                $values = array_merge($values, array_values(
                    $transaction->toArray()));
            }
            try {
                $stmt = $this->connection->prepare(
                    $query . implode(',', $placeHolers));
                $stmt->execute($values);
            } catch (\PDOException $e) {
                $this->connection->rollBack();
                throw $e;
            }
        }

        $this->connection->commit();
    }

    /**
     * Deletes all transactions in database.
     */
    public function deleteTransactions() : void
    {
        $this->connection->exec("DELETE FROM transactions;");
    }
}
