module Main exposing (main)

import BT exposing (..)
import Browser


main : Program () Model Msg
main =
    Browser.application
        { init =
            \_ url key ->
                ( initialModel url key
                , initialCmd
                )
        , view = view
        , update = update
        , subscriptions = always Sub.none
        , onUrlRequest = UrlChangeAttempt
        , onUrlChange = UrlChanged
        }
