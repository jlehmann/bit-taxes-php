module TimeUtils exposing (monthToInt, toUtcString)

import Time exposing (Month, millisToPosix, toDay, toHour, toMinute, toMonth, toSecond, toYear, utc)


monthToInt : Month -> Int
monthToInt month =
    case month of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12


toUtcString : Time.Posix -> String
toUtcString time =
    String.padLeft 4
        '0'
        (String.fromInt
            (toYear utc time)
        )
        ++ "-"
        ++ String.padLeft 2
            '0'
            (String.fromInt
                (toMonth utc time |> monthToInt)
            )
        ++ "-"
        ++ String.padLeft 2
            '0'
            (String.fromInt
                (toDay utc time)
            )
        ++ " "
        ++ String.padLeft 2
            '0'
            (String.fromInt
                (toHour utc time)
            )
        ++ ":"
        ++ String.padLeft 2
            '0'
            (String.fromInt
                (toMinute utc time)
            )
        ++ ":"
        ++ String.padLeft 2
            '0'
            (String.fromInt
                (toSecond utc time)
            )
