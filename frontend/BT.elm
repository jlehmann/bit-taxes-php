module BT exposing
    ( Model
    , Msg(..)
    , initialCmd
    , initialModel
    , update
    , view
    )

import Browser exposing (UrlRequest)
import Browser.Navigation as Nav
import File exposing (File)
import File.Select as Select
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events as Events
import Http
import Json.Decode as Decode exposing (Decoder, bool, int, list, string)
import Json.Decode.Pipeline exposing (optional, required)
import Task
import Time exposing (millisToPosix)
import TimeUtils exposing (..)
import Url exposing (Url)
import Url.Builder as UrlBuilder


type Msg
    = Noop
    | GetTransactions Int
    | GotTransactions (Result Http.Error TransactionResponse)
    | GetTaxReport
    | GotTaxReport (Result Http.Error TaxReport)
    | UrlChangeAttempt UrlRequest
    | UrlChanged Url
    | CsvRequested
    | CsvSelected File
    | CsvLoaded String String
    | CsvSent (Result Http.Error ())
    | SelectPage Page
    | SetTaxParams TaxParams


type Page
    = TransactionsPage
    | TaxesPage


type alias Transaction =
    { id : String
    , type_ : String
    , buy : Int
    , buyCurrency : String
    , sell : Int
    , sellCurrency : String
    , fee : Int
    , feeCurrency : String
    , exchange : String
    , group : String
    , comment : String
    , date : Int
    }


type TaxMethod
    = FIFO
    | LIFO


type TaxParams
    = TaxParams TaxMethod Int Int


type alias TaxReport =
    { method : TaxMethod
    , year : Int
    , sold : Int
    , percent : Int
    , cost : Int
    , proceeds : Int
    , gain : Int
    , tax : Int
    }


type alias Navigation =
    { key : Nav.Key
    , url : Url
    }


type alias TransactionResponse =
    { transactions : List Transaction
    , pages : Int
    , currentPage : Int
    }


type alias Model =
    { transactionResponse : Maybe TransactionResponse
    , taxReport : Maybe TaxReport
    , taxParams : TaxParams
    , error : Maybe Http.Error
    , loading : Bool
    , navigation : Navigation
    , activePage : Page
    }


satoshi : Float
satoshi =
    100000000.0


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Noop ->
            ( model, Cmd.none )

        GetTransactions page ->
            ( { model
                | loading = True
                , transactionResponse = Nothing
              }
            , getTransactions page
            )

        GotTransactions (Ok decodedTransactionResponse) ->
            ( { model
                | transactionResponse = Just decodedTransactionResponse
                , error = Nothing
                , loading = False
              }
            , Cmd.none
            )

        GotTransactions (Err error) ->
            ( { model
                | error = Just error
                , loading = False
              }
            , Cmd.none
            )

        CsvRequested ->
            ( model, Select.file [ "text/csv" ] CsvSelected )

        CsvSelected file ->
            ( model, Task.perform (CsvLoaded (File.name file)) (File.toString file) )

        CsvLoaded name content ->
            ( { model | loading = True }, postCsv name content )

        CsvSent (Ok _) ->
            ( { model
                | error = Nothing
                , loading = False
                , activePage = TransactionsPage
              }
            , getTransactions 1
            )

        CsvSent (Err error) ->
            ( { model
                | error = Just error
                , loading = False
              }
            , Cmd.none
            )

        SelectPage page ->
            ( { model | activePage = page }, Cmd.none )

        SetTaxParams taxParams ->
            ( { model | taxParams = taxParams }, Cmd.none )

        GetTaxReport ->
            ( { model
                | loading = True
                , taxReport = Nothing
              }
            , getTaxReport model.taxParams
            )

        GotTaxReport (Ok decodedTaxReport) ->
            ( { model
                | loading = False
                , taxReport = Just decodedTaxReport
              }
            , Cmd.none
            )

        GotTaxReport (Err error) ->
            ( { model
                | error = Just error
                , loading = False
              }
            , Cmd.none
            )

        UrlChangeAttempt urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Cmd.none )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            let
                navigation =
                    model.navigation
            in
            ( { model | navigation = { navigation | url = url } }, Cmd.none )


viewTransaction : Transaction -> Html Msg
viewTransaction transaction =
    let
        date =
            transaction.date |> millisToPosix |> toUtcString

        buy =
            String.fromFloat (toFloat transaction.buy / satoshi)

        sell =
            String.fromFloat (toFloat transaction.sell / satoshi)

        fee =
            String.fromFloat (toFloat transaction.fee / satoshi)
    in
    div [ class "transaction" ]
        [ div [ class "type" ] [ text transaction.type_ ]
        , div [ class "buy" ] [ text buy ]
        , div [ class "buy-currency" ] [ text transaction.buyCurrency ]
        , div [ class "sell" ] [ text sell ]
        , div [ class "sell-currency" ] [ text transaction.sellCurrency ]

        --  , div [ class "fee" ] [ text fee ]
        --  , div [ class "fee-currency" ] [ text transaction.feeCurrency ]
        --  , div [ class "exchange" ] [ text transaction.exchange ]
        --  , div [ class "comment" ] [ text transaction.comment ]
        , div [ class "date" ] [ text date ]
        ]


viewTransactions : Maybe TransactionResponse -> Html Msg
viewTransactions transactionResponse =
    let
        transactions =
            case transactionResponse of
                Just tr ->
                    tr.transactions

                Nothing ->
                    []

        currentPage =
            case transactionResponse of
                Just tr ->
                    tr.currentPage

                Nothing ->
                    0

        pages =
            case transactionResponse of
                Just tr ->
                    tr.pages

                Nothing ->
                    0

        header =
            div [ class "header" ]
                [ div [ class "name" ] [ text "Type" ]
                , div [ class "buy" ] [ text "Buy" ]
                , div [ class "buy-currency" ] [ text "Currency" ]
                , div [ class "sell" ] [ text "Sell" ]
                , div [ class "sell-currency" ] [ text "Currency" ]

                --, div [ class "fee" ] [ text "Fee" ]
                --, div [ class "fee-currency" ] [ text "Currency" ]
                --, div [ class "exchange" ] [ text "Exchange" ]
                --, div [ class "comment" ] [ text "Comment" ]
                , div [ class "date" ] [ text "Date" ]
                ]

        firstPage =
            if currentPage == 1 then
                []

            else
                [ li
                    [ Events.onClick (GetTransactions 1)
                    ]
                    [ text "first" ]
                ]

        lastPage =
            if currentPage == pages || pages <= 0 then
                []

            else
                [ li
                    [ Events.onClick (GetTransactions pages)
                    ]
                    [ text "last" ]
                ]

        prevPage =
            if currentPage - 1 <= 1 then
                []

            else
                [ li
                    [ Events.onClick (GetTransactions (currentPage - 1))
                    ]
                    [ text "prev" ]
                ]

        nextPage =
            if currentPage + 1 >= pages then
                []

            else
                [ li
                    [ Events.onClick (GetTransactions (currentPage + 1))
                    ]
                    [ text "next" ]
                ]

        pagination =
            div [ class "pagination" ]
                [ ul [ class "pagination" ]
                    (firstPage
                        ++ prevPage
                        ++ [ li
                                [ classList [ ( "active", True ) ]
                                ]
                                [ String.fromInt currentPage |> text ]
                           ]
                        ++ nextPage
                        ++ lastPage
                    )
                ]
    in
    div [ class "transactions" ]
        ([ header ]
            ++ List.map viewTransaction transactions
            ++ [ pagination ]
        )


viewTaxReport : TaxReport -> Html Msg
viewTaxReport taxReport =
    let
        method =
            case taxReport.method of
                FIFO ->
                    "FIFO"

                LIFO ->
                    "LIFO"

        sold =
            String.fromFloat (toFloat taxReport.sold / satoshi)

        cost =
            String.fromFloat (toFloat taxReport.cost / satoshi)

        proceeds =
            String.fromFloat (toFloat taxReport.proceeds / satoshi)

        gain =
            String.fromFloat (toFloat taxReport.gain / satoshi)

        tax =
            String.fromFloat (toFloat taxReport.tax / satoshi)
    in
    ul [ class "tax-results" ]
        [ li []
            [ span [] [ text "Method" ]
            , span [ class "tax-results-method" ]
                [ text method
                ]
            ]
        , li []
            [ span [] [ text "Year" ]
            , span []
                [ String.fromInt taxReport.year |> text
                ]
            ]
        , li []
            [ span [] [ text "Tax percent" ]
            , span [ class "tax-results-percent" ]
                [ String.fromInt taxReport.percent |> text
                ]
            ]
        , li []
            [ span [] [ text "Sold" ]
            , span [ class "tax-results-btc" ]
                [ text sold
                ]
            ]
        , li []
            [ span [] [ text "Cost" ]
            , span [ class "tax-results-usd" ]
                [ text cost
                ]
            ]
        , li []
            [ span [] [ text "Proceeds" ]
            , span [ class "tax-results-usd" ]
                [ text proceeds
                ]
            ]
        , li []
            [ span [] [ text "Gain" ]
            , span [ class "tax-results-usd" ]
                [ text gain
                ]
            ]
        , li []
            [ span [] [ text "Tax" ]
            , span [ class "tax-results-usd" ]
                [ text tax
                ]
            ]
        ]


viewTaxes : TaxParams -> Maybe TaxReport -> Html Msg
viewTaxes (TaxParams currentMethod currentPercent currentYear) maybeTaxReport =
    let
        taxReportView =
            case maybeTaxReport of
                Just taxReport ->
                    [ viewTaxReport taxReport ]

                Nothing ->
                    []
    in
    div [ class "taxes" ]
        ([ ul [ class "tax-params" ]
            [ li
                [ classList [ ( "active", currentMethod == FIFO ) ]
                , Events.onClick (SetTaxParams (TaxParams FIFO currentPercent currentYear))
                ]
                [ text "FIFO" ]
            , li
                [ classList [ ( "active", currentMethod == LIFO ) ]
                , Events.onClick (SetTaxParams (TaxParams LIFO currentPercent currentYear))
                ]
                [ text "LIFO" ]
            , li
                [ classList [ ( "active", currentPercent == 10 ) ]
                , Events.onClick (SetTaxParams (TaxParams currentMethod 10 currentYear))
                ]
                [ text "10%" ]
            , li
                [ classList [ ( "active", currentPercent == 12 ) ]
                , Events.onClick (SetTaxParams (TaxParams currentMethod 12 currentYear))
                ]
                [ text "12%" ]
            , li
                [ classList [ ( "active", currentPercent == 15 ) ]
                , Events.onClick (SetTaxParams (TaxParams currentMethod 15 currentYear))
                ]
                [ text "15%" ]
            , li
                [ classList [ ( "active", currentYear == 2018 ) ]
                , Events.onClick (SetTaxParams (TaxParams currentMethod currentPercent 2018))
                ]
                [ text "2018" ]
            , li
                [ classList [ ( "active", currentYear == 2019 ) ]
                , Events.onClick (SetTaxParams (TaxParams currentMethod currentPercent 2019))
                ]
                [ text "2019" ]
            , li
                [ class "tax-generate"
                , Events.onClick GetTaxReport
                ]
                [ text "Generate" ]
            ]
         ]
            ++ taxReportView
        )


viewNav : Page -> Html Msg
viewNav currentPage =
    let
        isActive page =
            page == currentPage
    in
    ul [ class "nav" ]
        [ li
            [ classList [ ( "nav", True ), ( "active", isActive TransactionsPage ) ]
            , SelectPage TransactionsPage |> Events.onClick
            ]
            [ text "Transactions" ]
        , li
            [ classList [ ( "nav", True ), ( "active", isActive TaxesPage ) ]
            , SelectPage TaxesPage |> Events.onClick
            ]
            [ text "Taxes" ]
        ]


viewLoading : Bool -> Html Msg
viewLoading loading =
    div
        [ classList
            [ ( "not-loading-container", not loading )
            , ( "loading-container", loading )
            ]
        ]
        [ div
            [ classList
                [ ( "not-loading", not loading )
                , ( "loading", loading )
                ]
            ]
            []
        ]


viewImport : Html Msg
viewImport =
    div [ class "import" ]
        [ div [ Events.onClick CsvRequested ] [ text "Import" ]
        ]


viewError : Maybe Http.Error -> List (Html Msg)
viewError error =
    case error of
        Just e ->
            [ div [ class "error" ]
                [ div [] [ text "Something went wrong :)" ] ]
            ]

        Nothing ->
            []


viewBody : Model -> Html Msg
viewBody model =
    let
        page =
            case model.activePage of
                TransactionsPage ->
                    viewTransactions model.transactionResponse

                TaxesPage ->
                    viewTaxes model.taxParams model.taxReport
    in
    div [ class "content" ]
        [ div [ class "top" ]
            ([ viewLoading model.loading
             , viewNav model.activePage
             , viewImport
             ]
                ++ viewError model.error
            )
        , div [ class "page" ] [ page ]
        ]


view : Model -> Browser.Document Msg
view model =
    { title = "bt"
    , body = [ viewBody model ]
    }


transactionDecoder : Decoder Transaction
transactionDecoder =
    Decode.succeed Transaction
        |> required "id" string
        |> required "type" string
        |> required "buy" int
        |> required "buy_currency" string
        |> required "sell" int
        |> required "sell_currency" string
        |> required "fee" int
        |> required "fee_currency" string
        |> required "exchange" string
        |> required "group" string
        |> required "comment" string
        |> required "date" int


transactionsDecoder : Decoder (List Transaction)
transactionsDecoder =
    list transactionDecoder


transactionResponseDecoder : Decoder TransactionResponse
transactionResponseDecoder =
    Decode.succeed TransactionResponse
        |> required "transactions" transactionsDecoder
        |> required "pages" int
        |> required "current_page" int


taxMethodDecoder : Decoder TaxMethod
taxMethodDecoder =
    string
        |> Decode.andThen
            (\method ->
                case method of
                    "FIFO" ->
                        Decode.succeed FIFO

                    "LIFO" ->
                        Decode.succeed LIFO

                    invalidMethod ->
                        Decode.fail <| "Wrong tax method: " ++ invalidMethod
            )


taxReportDecoder : Decoder TaxReport
taxReportDecoder =
    Decode.succeed TaxReport
        |> required "method" taxMethodDecoder
        |> required "year" int
        |> required "sold" int
        |> required "percent" int
        |> required "cost" int
        |> required "proceeds" int
        |> required "gain" int
        |> required "tax" int


initialModel : Url -> Nav.Key -> Model
initialModel url key =
    { error = Nothing
    , transactionResponse = Nothing
    , taxReport = Nothing
    , taxParams = TaxParams FIFO 10 2018
    , loading = True
    , navigation = { url = url, key = key }
    , activePage = TransactionsPage
    }


initialCmd : Cmd Msg
initialCmd =
    Cmd.batch
        [ getTransactions 1 ]


getTransactions page =
    Http.get
        { url =
            UrlBuilder.absolute [ "api", "transactions" ]
                [ UrlBuilder.int "page" page ]
        , expect = Http.expectJson GotTransactions transactionResponseDecoder
        }


postCsv name content =
    Http.post
        { url = UrlBuilder.absolute [ "api", "csv" ] []
        , expect = Http.expectWhatever CsvSent
        , body = Http.stringBody "text/csv" content
        }


getTaxReport (TaxParams method percent year) =
    let
        selectedMethod =
            case method of
                FIFO ->
                    "FIFO"

                LIFO ->
                    "LIFO"
    in
    Http.get
        { url =
            UrlBuilder.absolute [ "api", "tax-report" ]
                [ UrlBuilder.string "method" selectedMethod
                , UrlBuilder.int "percent" percent
                , UrlBuilder.int "year" year
                ]
        , expect = Http.expectJson GotTaxReport taxReportDecoder
        }
