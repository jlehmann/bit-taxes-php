<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Model\TaxReport;
use App\Model\Transaction;

class TaxReportTest extends TestCase
{
    private $transactions;

    public function setUp() : void
    {
        $this->transactions = [
            Transaction::fromCsvRow(['Trade', '10100.00000000', 'USD',
                '0.50000000', 'BTC', '', '', '', '', '', '2019-09-15 17:24:19']),
            Transaction::fromCsvRow(['Trade', '10200.00000000', 'USD',
                '1.00000000', 'BTC', '', '', '', '', '', '2019-09-15 14:58:29']),
            Transaction::fromCsvRow(['Trade', '1.00000000', 'BTC', '5000.00000000',
                'USD', '', '', '', '', '', '2019-06-23 17:24:19']),
            Transaction::fromCsvRow(['Trade', '1.00000000', 'BTC', '10000.00000000',
                'USD', '', '', '', '', '', '2019-06-22 14:53:00']),
        ];
    }

    /**
     * @covers App\Model\TaxReport::calculate
     */
    public function testFIFOTaxReport() : void
    {
        $taxReport = new TaxReport('FIFO', 10, 2019);
        $taxReport->calculate($this->transactions);

        $this->assertEquals(78000000000, $taxReport->getTax());

        $taxReport = new TaxReport('FIFO', 10, 2018);
        $taxReport->calculate($this->transactions);

        $this->assertEquals(0, $taxReport->getTax());

        $taxReport = new TaxReport('FIFO', 15, 2019);
        $taxReport->calculate($this->transactions);

        $this->assertEquals(117000000000, $taxReport->getTax());
    }

    /**
     * @covers App\Model\TaxReport::calculate
     */
    public function testLIFOTaxReport() : void
    {
        $taxReport = new TaxReport('LIFO', 10, 2019);
        $taxReport->calculate($this->transactions);

        $this->assertEquals(103000000000, $taxReport->getTax());

        $taxReport = new TaxReport('LIFO', 10, 2018);
        $taxReport->calculate($this->transactions);

        $this->assertEquals(0, $taxReport->getTax());

        $taxReport = new TaxReport('LIFO', 15, 2019);
        $taxReport->calculate($this->transactions);

        $this->assertEquals(154500000000, $taxReport->getTax());
    }
}
