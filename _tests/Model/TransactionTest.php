<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Model\Transaction;

class TransactionTest extends TestCase
{
    private $validCsvRow;
    private $invalidCsvRow;
    private $validArray;

    public function setUp() : void
    {
        $this->validCsvRow = [
            'Trade',
            '10100.00000000',
            'USD',
            '0.50000000',
            'BTC',
            '',
            '',
            '',
            '',
            '',
            '2019-09-15 17:24:19',
        ];
        $this->validArray = [
            'type' => 'Trade',
            'buy' => 1010000000000.0,
            'buy_currency' => 'USD',
            'sell' => 50000000.0,
            'sell_currency' => 'BTC',
            'fee' => 0,
            'fee_currency' => '',
            'exchange' => '',
            'group' => '',
            'comment' => '',
            'date' => 1568568259000,
        ];
        $this->validResultRow = $this->validArray;
        $this->validResultRow['group_'] = $this->validResultRow['group'];
        $this->validResultRow['id'] = 'rowid';
        unset($this->validResultRow['group']);
        $this->invalidCsvRow = [];
    }

    /**
     * @covers App\Model\Transaction::fromCsvRow
     */
    public function testCanBeCreatedFromValidCsvRow() : void
    {
        $this->assertInstanceOf(
            Transaction::class,
            Transaction::fromCsvRow($this->validCsvRow)
        );
    }

    /**
     * @covers App\Model\Transaction::fromCsvRow
     */
    public function testCannotBeCreatedFromInvalidCsvRow() : void
    {
        $this->expectException(Exception::class);
        Transaction::fromCsvRow($this->invalidCsvRow);
    }

    /**
     * @covers App\Model\Transaction::fromResultRow
     */
    public function testCanBeCreatedFromValidResultRow() : void
    {
        $this->assertInstanceOf(
            Transaction::class,
            Transaction::fromResultRow($this->validResultRow)
        );
    }

    /**
     * @covers App\Model\Transaction::toArray
     */
    public function testConvertsToArray() : void
    {
        //ignore random id
        $transaction = Transaction::fromCsvRow($this->validCsvRow);
        unset($transaction->id);

        $this->assertEquals(
            $this->validArray,
            $transaction->toArray()
        );
    }
}
