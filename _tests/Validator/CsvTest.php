<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Validator\Csv as Validator;

class CsvTest extends TestCase
{
    private $validCsvRow;
    private $invalidCsvRow;

    public function setUp() : void
    {
        $this->validCsvRow = [
            'Trade',
            '10100.00000000',
            'USD',
            '0.50000000',
            'BTC',
            '',
            '',
            '',
            '',
            '',
            '2019-09-15 17:24:19',
        ];
        $this->invalidCsvRow = [];
    }

    /**
     * @covers App\Validator\Csv::isValidCsvRow()
     */
    public function testValidatesValidCsvRow() : void
    {
        $this->assertTrue(Validator::isValidCsvRow($this->validCsvRow));
    }

    /**
     * @covers App\Validator\Csv::isValidCsvRow()
     */
    public function testInvalidatesInvalidCsvRow() : void
    {
        $this->assertFalse(Validator::isValidCsvRow($this->invalidCsvRow));
    }
}
