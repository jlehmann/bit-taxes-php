<?php
use Slim\Factory\AppFactory;
use App\Controller\TransactionsController;
use App\Controller\CsvController;
use App\Controller\TaxReportController;
use App\Config\Config;
use App\Data\DB;
use DI\Container;

require __DIR__ . '/../vendor/autoload.php';

$conteiner = new Container();
$conteiner->set('db', new DB(Config::$DB_FILE));

AppFactory::setContainer($conteiner);
$app = AppFactory::create();

$app->get('/api/transactions', TransactionsController::class);
$app->post('/api/csv', CsvController::class);
$app->get('/api/tax-report', TaxReportController::class);

$app->run();
