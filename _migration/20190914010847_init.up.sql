CREATE TABLE transactions (
    id TEXT PRIMARY KEY,
    type TEXT NOT NULL,
    buy INTEGER,
    buy_currency TEXT,
    sell INTEGER,
    sell_currency TEXT,
    fee INTEGER,
    fee_currency TEXT,
    exchange TEXT,
    group_ TEXT,
    comment TEXT,
    date INTEGER NOT NULL
);
