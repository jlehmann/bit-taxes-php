DOCKER_FILE = _docker/Dockerfile
DOCKER_IMAGE = bit-taxes-php
ELM_OBJ = $(find -iname "*.elm")
FRONTEND_DIR = frontend
OUTPUT_DIR = public/js
APP_NAME = bt
PHP_UNIT = ./vendor/bin/phpunit
DATABASE_FILE = database/storage.sqlite

.PHONY: build-elm
build-elm: $(ELM_OBJ)
	elm make ${FRONTEND_DIR}/Main.elm --output ${OUTPUT_DIR}/${APP_NAME}.js
    
.PHONY: build-elm-opt
build-elm-opt: $(ELM_OBJ)
	elm make ${FRONTEND_DIR}/Main.elm --optimize --output ${OUTPUT_DIR}/${APP_NAME}.js
	uglifyjs ${OUTPUT_DIR}/${APP_NAME}.js --output=${OUTPUT_DIR}/${APP_NAME}.js \
	    --compress "pure_funcs=[F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9],pure_getters,keep_fargs=false,unsafe_comps,unsafe"
	uglifyjs ${OUTPUT_DIR}/${APP_NAME}.js --mangle --output=${OUTPUT_DIR}/${APP_NAME}.js

.PHONY: build-docker
build-docker:
	docker build -t $(DOCKER_IMAGE) -f $(DOCKER_FILE) .
	
.PHONY: run-docker
run-docker: build-docker
	docker run -it -p 8080:80 $(DOCKER_IMAGE)

.PHONY: up
up:
	docker-compose up -d
	
.PHONY: down
down:
	docker-compose down

.PHONY: initdb
initdb:
	rm -f ${DATABASE_FILE}
	mkdir $(shell dirname ${DATABASE_FILE}) || true
	for query in $(shell ls _migration/*up*); do cat "$$query" | sqlite3 ${DATABASE_FILE}; done
	chmod a+rw ${DATABASE_FILE}
	chmod a+rwX $(shell dirname ${DATABASE_FILE})

.PHONY: test
test:
	${PHP_UNIT} --bootstrap vendor/autoload.php _tests
