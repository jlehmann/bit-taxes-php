## Project name:
bit-taxes-php

## Description:
The purpose of this application is to store bitcoin/cash transactions and to calculate taxes based on them.

The first and default page (Transactions) consists of the paginated transactions table and the transactions import button, transactions are ordered from last to first.

Import itself should be in the CSV format.

The other page (Taxes) displays a parametrization panel and a "Generate" button. Available tax calculation parameters are as follows: FIFO/LIFO taxation methods; 10, 12 and 15 percent tax; current and previous tax year.
Generated tax report displays chosen taxation parameters, overall sold BTC, cost of the sold BTC, proceeds, gains and final tax.

The application supports only BTC and USD dollars.

## Sample supported CSV file:
```
"Type","Buy","Cur.","Sell","Cur.","Fee","Cur.","Exchange","Group","Comment","Date"
"Trade","10100.00000000","USD","1.00000000","BTC","","","","","","2019-09-15 17:24:19"
"Trade","1.00000000","BTC","5000.00000000","USD","","","","","","2019-06-23 17:24:19"
```

## Installation:
To run this project locally *docker* installation is required.
If host system has *make* one can just call:
```
make run-docker
```
in the root directory of the project.

In case there is no *make* available, one needs to issue following commands in the root project directory:
```
docker build -t bit-taxes-php -f _docker/Dockerfile .
docker run -it -p 8080:80 bit-taxes-php
```

Application will be available under: http://localhost:8080
